import {
  createAsyncThunk,
  createSlice,
  createEntityAdapter
} from '@reduxjs/toolkit';
import { ImagesAPI } from 'src/features/images/utils/imagesAPI.js';

const imagesAdaptor = createEntityAdapter();

const initialState = imagesAdaptor.getInitialState({
  fetchingStaus: 'idle',
  error: null
});

// Axios based class wrapper for intercting with the API
const imagesAPI = new ImagesAPI();

export const fetchAllImages = createAsyncThunk(
  'images/fetchAll',
  async (payload, { rejectWithValue }) => {
    try {
      const { data } = await imagesAPI.fetchAllImages();
      return data;
    } catch (error) {
      return rejectWithValue(error.response);
    }
  }
);

export const fetchImageFaces = createAsyncThunk(
  'images/fetchImageFaces',
  async (payload, { rejectWithValue }) => {
    const { id } = payload;
    try {
      const { data } = await imagesAPI.fetchImageFaces(id);
      return data;
    } catch (error) {
      return rejectWithValue(error.response);
    }
  }
);

export const imagesSlice = createSlice({
  name: 'images',
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(fetchAllImages.fulfilled, (state, action) => {
        imagesAdaptor.upsertMany(state, action.payload);
        state.fetchingStaus = 'fulfilled';
      })
      .addCase(fetchImageFaces.fulfilled, (state, action) => {
        imagesAdaptor.updateOne(state, {
          id: action.meta.arg.id,
          changes: {
            faces: action.payload
          }
        });
        state.fetchingStaus = 'fulfilled';
      })
      .addMatcher(
        (action) => action.type.endsWith('/pending'),
        (state) => {
          state.fetchingStaus = 'pending';
        }
      )
      .addMatcher(
        (action) => action.type.endsWith('/rejected'),
        (state) => {
          state.fetchingStaus = 'rejected';
        }
      );
  }
});

export const {
  selectById: selectImageById,
  selectIds: selectImageIds,
  selectEntities: selectImageEntities,
  selectAll: selectAllImages,
  selectTotal: selectTotalImages
} = imagesAdaptor.getSelectors((state) => state.images);

export default imagesSlice.reducer;
