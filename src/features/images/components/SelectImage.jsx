import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Image as MantineImage, Box } from '@mantine/core';
import { useElementSize } from '@mantine/hooks';
import * as PropTypes from 'prop-types';

import { fetchImageFaces } from 'src/features/images/store/imagesSlice.js';
import { FaceBox } from './FaceBox.jsx';

/**
 * An image with detected faces.
 * @param props
 * @param {string} props.id
 * @param {string} props.url
 * @param {string} props.filename
 * @param {Array} props.faces
 * @returns {JSX.Element}
 */
export function SelectImage({ id, url, filename, faces }) {
  const dispatch = useDispatch();
  const { ref, width, height } = useElementSize();

  useEffect(() => {
    if (id) dispatch(fetchImageFaces({ id }));
  }, [id]);

  return (
    <Box>
      {faces?.map(({ id, ...coordinates }) => (
        <FaceBox
          key={id}
          id={id}
          coordinates={coordinates}
          imageDimensions={{ width, height }}
        />
      ))}
      <MantineImage
        styles={{
          root: {
            display: 'flex'
          }
        }}
        imageProps={{
          ref,
          alt: filename
        }}
        src={url}
      ></MantineImage>
    </Box>
  );
}

SelectImage.propTypes = {
  id: PropTypes.string,
  url: PropTypes.string,
  filename: PropTypes.string,
  faces: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      xmin: PropTypes.number,
      xmax: PropTypes.number,
      ymin: PropTypes.number,
      ymax: PropTypes.number
    })
  )
};
