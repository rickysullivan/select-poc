import { useState } from 'react';
import { Box, Menu } from '@mantine/core';
import * as PropTypes from 'prop-types';

/**
 * Displays a box over a face
 * @param {Object} coordinates
 * @param {Number} coordinates.xmin
 * @param {Number} coordinates.xmax
 * @param {Number} coordinates.ymax
 * @param {Number} coordinates.ymax
 * @param {Object} imageDimensions
 * @param {Number} imageDimensions.width
 * @param {Number} imageDimensions.height
 * @returns {JSX.Element}
 */
export function FaceBox({ coordinates, imageDimensions }) {
  const [activeFace, setActiveFace] = useState(false);
  return (
    <Menu
      onOpen={() => setActiveFace(true)}
      onClose={() => setActiveFace(false)}
      shadow="md"
      radius={0}
      styles={{
        dropdown: { padding: '0 !important' },
        divider: { margin: 0 },
        item: {
          color: '#828282',
          '&:hover': {
            backgroundColor: '#000000',
            color: '#FFFFFF'
          }
        }
      }}
    >
      <Menu.Target>
        <Box
          className={activeFace ? 'active' : ''}
          sx={{
            position: 'absolute',
            top: `${
              (100 / imageDimensions.height) *
              (coordinates?.ymin * imageDimensions.height)
            }%`,
            left: `${coordinates?.xmin * imageDimensions.width}px`,
            right: `${
              imageDimensions.width - coordinates?.xmax * imageDimensions.width
            }px`,
            bottom: `${
              100 -
              (100 / imageDimensions.height) *
                coordinates?.ymax *
                imageDimensions.height
            }%`,
            border: '1px solid white',
            zIndex: 2,
            '&:hover': {
              borderColor: '#EB5757',
              cursor: 'pointer'
            },
            '&.active': {
              borderColor: '#EB5757',
              cursor: 'pointer'
            }
          }}
        />
      </Menu.Target>
      <Menu.Dropdown>
        <Menu.Item>Dummy Item</Menu.Item>
        <Menu.Divider />
        <Menu.Item>This Does Nothing</Menu.Item>
        <Menu.Divider />
        <Menu.Item>Fake News</Menu.Item>
        <Menu.Divider />
        <Menu.Item
          color="red"
          sx={{
            '&:hover': {
              backgroundColor: '#EB5757',
              cursor: 'pointer',
              color: '#FFFFFF'
            }
          }}
        >
          Delete Frame
        </Menu.Item>
      </Menu.Dropdown>
    </Menu>
  );
}

FaceBox.propTypes = {
  coordinates: PropTypes.shape({
    xmin: PropTypes.number,
    xmax: PropTypes.number,
    ymin: PropTypes.number,
    ymax: PropTypes.number
  }),
  imageDimensions: PropTypes.shape({
    width: PropTypes.number,
    height: PropTypes.number
  })
};
