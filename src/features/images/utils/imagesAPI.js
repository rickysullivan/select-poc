import axios from 'axios'

class ImagesAPI {
  constructor() {
    this.axoisClient = axios.create({
      baseURL: 'https://cully-api.herokuapp.com/',
    })
  }

  fetchAllImages() {
    return this.axoisClient.get('images').then((response) => response.data)
  }

  fetchImageFaces(id) {
    return this.axoisClient
      .get(`images/${id}/faces`)
      .then((response) => response.data)
  }
}

export { ImagesAPI }
