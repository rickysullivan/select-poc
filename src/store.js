import { configureStore } from '@reduxjs/toolkit';
import { imagesSlice } from 'src/features/images/store/imagesSlice.js';

export const store = configureStore({
  reducer: {
    images: imagesSlice.reducer
  }
});
