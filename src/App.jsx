import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Box, Center } from '@mantine/core';
import { Carousel } from '@mantine/carousel';

import {
  fetchAllImages,
  selectImageEntities
} from 'src/features/images/store/imagesSlice.js';
import { SelectImage } from 'src/features/images/components/SelectImage.jsx';

function App() {
  const dispatch = useDispatch();
  const imagesFetchingStatus = useSelector(
    (state) => state.images.fetchingStaus
  );
  const images = Object.values(useSelector(selectImageEntities));

  useEffect(() => {
    dispatch(fetchAllImages());
  }, []);

  return (
    <Container fluid px={0}>
      {imagesFetchingStatus === 'rejected' ? (
        <>Whoops, something bad happened.</>
      ) : images ? (
        <Center
          sx={{
            height: '100vh',
            display: 'flex',
            padding: '20px 40px'
          }}
        >
          {imagesFetchingStatus === 'pending' && images.length === 0 && (
            <Box sx={{ textAlign: 'center', position: 'absolute' }}>
              Loading...
            </Box>
          )}
          <Carousel
            withIndicators={true}
            draggable={false}
            sx={{ display: 'flex', flex: 1, justifyContent: 'center' }}
            height="100%"
          >
            {images?.map(({ id, url, filename, faces }) => (
              <Carousel.Slide key={id}>
                <SelectImage
                  id={id}
                  url={url}
                  filename={filename}
                  faces={faces}
                />
              </Carousel.Slide>
            ))}
          </Carousel>
        </Center>
      ) : null}
    </Container>
  );
}

export default App;
